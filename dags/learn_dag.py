import pandas as pd
from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
import os

tmp_dir = '/usr/local/airflow/tmp'
os.makedirs(tmp_dir, exist_ok=True)

def download_data():
    # downloading data sample
    print("....In download_data function....")
    data = {
        'id': [1, 2, 3, 4],
        'name': ['Ishte', 'Alam', 'Ishteaque', 'karim'],
        'age': [30, 35, 25, 17]
    }
    df = pd.DataFrame(data)
    df.to_csv(os.path.join(tmp_dir, 'downloaded_data.csv'), index=False)

def extract_features():
    print("....In extract_features function....")
    # Dummy feature extraction
    df = pd.read_csv(os.path.join(tmp_dir, 'downloaded_data.csv'))
    df['is_adult'] = df['age'] >= 18
    df.to_csv(os.path.join(tmp_dir, 'featured_data.csv'), index=False)

def process_data():
    # Dummy data processing
    print("....In process_data function....")
    df = pd.read_csv(os.path.join(tmp_dir, 'featured_data.csv'))
    processed_df = df[df['is_adult']]
    processed_df.to_csv(os.path.join(tmp_dir, 'processed_data.csv'), index=False)

default_args = {
    'owner': 'ishteaque',
    'depends_on_past': False,
    'start_date': datetime(2024, 2, 1),
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=1),
}

dag = DAG(
    'learn_dag',
    default_args=default_args,
    description='A DAG to demonstrate data processing steps',
    schedule_interval=timedelta(days=1),
    
)

task1 = PythonOperator(
    task_id='download_data',
    python_callable=download_data,
    dag=dag,
)

task2 = PythonOperator(
    task_id='extract_features',
    python_callable=extract_features,
    dag=dag,
)

task3 = PythonOperator(
    task_id='process_data',
    python_callable=process_data,
    dag=dag,
)

task4 = PythonOperator(
    task_id='save_as_csv',
    python_callable=lambda: print("Data saved as CSV"),
    dag=dag,
)
## Define task dependencis/Workflow management/MLOps
task1 >> task2 >> task3 >> task4