try:

    from datetime import timedelta
    from airflow import DAG
    from airflow.operators.python_operator import PythonOperator
    from datetime import datetime
    import pandas as pd

    print("All Dag modules are loaded ...")
except Exception as e:
    print("Error  {} ".format(e))


def first_function_execute(**context):
    print("first_function_execute   ")
    op_kwargs_value = context['name']
    print("In frist funtion - op_kwargs_value ", op_kwargs_value)
    context['ti'].xcom_push(key='key_1', value="irst_function_execute_VALUE")


def second_function_execute(**context):
    print("Inside second function!")
    instance = context.get("ti").xcom_pull(key="key_1")
    data = [{"name":"Mr ABC","title":"Senior Manager"}, { "name":"Mr XYZ","title":"Software Engineer"},]
    df = pd.DataFrame(data=data)
    print('@'*66)
    print(df.head())
    print('@'*66)

    print("This is the second_function_execute, got value :{} from the First Function ".format(instance))


# */5 * * * * cronjob every 5 minute
with DAG(
        dag_id="first_dag",
        schedule_interval="@daily",
        default_args={
            "owner": "airflow",
            "retries": 1,
            "retry_delay": timedelta(minutes=5),
            "start_date": datetime(2021, 1, 1),
        },
        catchup=False) as f:

    first_function_execute = PythonOperator(
        task_id="first_function_execute",
        python_callable=first_function_execute,
        provide_context=True,
        op_kwargs={"name":"Mr Query"} # query params
    )

    second_function_execute = PythonOperator(
        task_id="second_function_execute",
        python_callable=second_function_execute,
        provide_context=True,
    )

first_function_execute >> second_function_execute